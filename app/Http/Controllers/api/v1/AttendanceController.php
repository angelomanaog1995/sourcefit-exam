<?php
namespace App\Http\Controllers\api\v1;

use Carbon\Carbon;
use App\Models\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AttendanceController extends Controller
{
    public function saveLog(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'date' => ['required','date'],
            'time_in' => ['required','date_format:H:i'],
            'time_out' => ['required','date_format:H:i','after:time_in']
        ]);

        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Errors.',
                'errors' => $validation->errors()
            ], 401);
        }

        $attendance = Attendance::create([
            'user_id' => auth()->user()->id,
            'date' => $request->date,
            'time_in' => $request->time_in,
            'time_out' => $request->time_out,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Date and Time has been saved successfully.',
            'data' => ['userAttendanceLog' =>  $attendance]
        ], 200);
    }

    protected static function hoursToFloat($hour)
    {
        $hourArr = explode(':',$hour);
        return $hourArr[0] + floor(($hourArr[1]/60)*100) / 100;
    }

    public function getUserAttendances()
    {
        $attendances = Attendance::with('user')->orderBy('date','desc')->get();
        $attendanceArr = [];

        foreach ($attendances as $key => $attendance) 
        {
            $attendanceArr[$key]['name'] = $attendance->user->last_name.', '.$attendance->user->first_name;
            $attendanceArr[$key]['date'] = $attendance->date->format('m-d-Y');
            $attendanceArr[$key]['time_in'] = $attendance->time_in->format('h:i a');
            $attendanceArr[$key]['time_out'] = $attendance->time_out->format('h:i a');

            $timeDiff = $attendance->time_in->diff($attendance->time_out)->format('%H:%I');
            $totalHours = self::hoursToFloat($timeDiff);
            $hourBreak = 1;
            $regularHour = 8;
            $hoursWork = ($totalHours < $regularHour) ? $totalHours : $totalHours - $hourBreak;
            $hoursUndertime = ($hoursWork < $regularHour) ? $regularHour - $hoursWork : 0;

            $attendanceArr[$key]['hours_worked'] = $hoursWork;
            $attendanceArr[$key]['hours_overtime'] = ($hoursWork > $regularHour) ? $hoursWork - $regularHour : 0;
            $attendanceArr[$key]['hours_undertime'] = $hoursUndertime;
            $attendanceArr[$key]['hours_late'] =  0; 
        }

        return response()->json([
            'success' => true,
            'message' => 'User Logs has been displayed successfully.',
            'data' => ['userAttendanceLogs' =>  $attendanceArr]
        ], 200);
    }
}