<?php
namespace App\Http\Controllers\api\v1;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => ['email','required'],
            'password' => ['required']
        ]);

        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Errors.',
                'errors' => $validation->errors()
            ], 401);
        } 

        if (!auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid Credentials.'
            ], 401);
        }

        $authUser = auth()->user(); 

        return response()->json([
            'success' => true,
            'message' => 'Login Success.',
            'data' => [
                'user' =>  $authUser,
                'token' => $authUser->createToken('sourcefit')->plainTextToken
            ]
        ], 200);
    }

    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'first_name' => ['required','max:100'],
            'last_name' => ['required','max:100'],
            'email' => ['email','required','unique:users,email'],
            'password' => ['required','confirmed']
        ]);

        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation Errors.',
                'errors' => $validation->errors()
            ], 401);
        }

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'middle_name' => $request->middle_name,
            'name' => $request->first_name.' '.$request->middle_name.' '.$request->last_name,
            'email' => $request->email,
            'contact_number' => $request->contact_number,
            'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Signup Successful.',
            'data' => [
                'user' =>  $user,
                'token' => $user->createToken('sourcefit')->plainTextToken
            ]
        ], 200);
    }
}