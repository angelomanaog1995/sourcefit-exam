<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $guarded = ['id'];
    public $timestamps = false;
    protected $dates = [
        'date',
        'time_in',
        'time_out'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}