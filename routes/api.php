<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\api\v1\AuthController;
use App\Http\Controllers\api\v1\AttendanceController;

Route::prefix('v1')->name('api.v1.')->group(function () {
    Route::post('/login', [AuthController::class, 'login'])->name('user.login');
    Route::post('/register', [AuthController::class, 'register'])->name('user.register');
});

Route::prefix('v1')->name('api.v1.')->middleware('auth:sanctum')->group(function () {
    Route::post('/attendance/saveLog', [AttendanceController::class, 'saveLog'])->name('attendance.saveLog');
    Route::get('/attendances/user', [AttendanceController::class, 'getUserAttendances'])->name('attendance.getUserAttendances');
});