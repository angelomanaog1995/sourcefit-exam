<?php

namespace Tests\Feature;

use DatabaseMigrations;
use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AttendanceApiTest extends TestCase
{
    use RefreshDatabase;
    
    public function test_can_create_user_attendance_log()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        $formData = [
            'date' => '2022-01-29',
            'time_in' => '08:00',
            'time_out' => '17:00'
        ];

        $this->post(route('api.v1.attendance.saveLog'), $formData)
            ->assertStatus(200);
    }

    public function test_can_display_user_attendance_logs()
    {
        $user = User::factory()->create();
        Sanctum::actingAs($user);

        $this->get(route('api.v1.attendance.getUserAttendances'))->assertStatus(200);
    }
}
