<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Database\Eloquent\Factories\Factory;
use Tests\TestCase;

class UserApiTest extends TestCase
{
   
    public function test_can_create_user()
    {
        $faker = \Faker\Factory::create();
        $formData = [
            'first_name' => $faker->firstname(),
            'last_name' => $faker->lastname(),
            'middle_name' => $faker->lastname(),
            'email' => $faker->unique()->safeEmail(),
            'password' => 'password',
            'password_confirmation' => 'password',
            'contact_number' => $faker->phoneNumber(),
        ];

        $this->post(route('api.v1.user.register'), $formData)
            ->assertStatus(200);
    }
}
